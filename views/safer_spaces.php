<!-- Main -->
<div id="main">
  <article class="post featured policy">
    <header class="major">
      <h2>Safer Spaces Policy</h2>
    </header>

    <p><a href="https://s3-ap-southeast-2.amazonaws.com/grec-froc-m5x2-5fgg67/assets/resources/Safer+Spaces+Petroleum+Summit.pdf" target="_blank">link to download as pdf</a></p>

    <p><b><i>Background for policy:</i></b></p>
    <ul>
      <li>The organising group for the Rally for Climate Justice aims to provide a space that is welcoming to all types of people.</li>
      <li>A safer space seeks to critique and dismantle oppressive power structures both within the organising space itself (at a rally or meeting, for example), and in the wider world outside the space.</li>
      <li>Forms of oppression, domination and discrimination include but are not limited to: racism, colonialism, patriarchy, sexism, homophobia, transphobia, transmisogyny, ableism, classism, fatphobia, and ageism.</li>
      <li>In keeping with the rally principles, we are committed to creating safer spaces for those most affected by climate change, including tangata whenua and other Indigenous Peoples, people of colour, women, children, the working class and the economically marginalised.</li>
      <li>Our organising spaces are located on colonised land. We acknowledge mana whenua as the rightful kaitiaki of Aotearoa, and recognise that tino rangatiratanga was never ceded.</li>
    </ul>

    <p><b><i>Covered in this Policy:</i></b></p>
    <ul>
      <li>Interactions between those attending the Rally for Climate Justice on March 26 and 27, as well as interactions at associated events, such as actions, meetings, NVDA trainings and briefings.</li>
      <li>Interactions between those using the provided accommodation for the rally (this covers both activists who are being billeted and those hosted at the Cook Islands Society Hall).</li>
      <li>Communications between activists via phone, email and social media channels (e.g. Facebook).</li>
    </ul>

    <p><b><i>Not Covered:</i></b></p>
    <ul>
      <li>Interactions with third parties such as police and security.</li>
    </ul>

    <p><b><i>Safer Spaces and the Rally for Climate Justice:</i></b></p>
    <ul>
      <li>We want the Rally and associated events to be family-friendly and fun with a positive atmosphere.</li>
      <li>Organisers and volunteers will be available on March 27th to ensure that the event is as safe and accessible as possible. </li>
      <li>The ‘Chill Zone’ will offer a calm and supportive environment away from the rally. We will have a phone available to contact a range of support services if needed. </li>
      <li>Safer Space volunteers will be visible with orange safer spaces vests. They will be available to approach throughout the day if you feel unsafe and need support.</li>
      <li>Please refrain from wearing or demonstrating any symbols that may intimidate or offend other participants, e.g. Nazi Swastikas.</li>
      <li>No weapon or item that appears to be a weapon may be brought or used on the various sites of our actions.</li>
    </ul>

    <p><b><i>Safer Spaces Policy in action:</i></b></p>
    <ul>
      <li>If someone approaches a safer spaces volunteer or a rally organiser with an issue, they have a responsibility to try to resolve it. If the complainant’s consent is obtained, they may bring the issue to the rally organising group.</li>
      <li>The rally organising group makes decisions by consensus, therefore this will be the method of deciding whether people should leave the space in question (such as the shared accommodation or the rally itself).</li>
      <li>We will normally give participants a chance to change or address their behaviour or language if they have made others feel uncomfortable, threatened, or unsafe. If the person is refuses to do so they will be asked to leave the space.</li>
      <li>However, depending on the nature of the complaint (such as sexual assault), a warning may not be deemed an acceptable solution and the person may be asked to leave immediately.</li>
      <li>When a rapid decision needs to be made, a consensus process will involve as many group members as possible in the circumstances.</li>
      <li>The person being asked to leave may be included in this conversation unless the group decides to ask them to sit it out.</li>
      <li>For the purposes of these two decisions (asking someone to leave, and whether they can take part in the conversation), the group does not require the agreement of the person(s) who has caused the issue in question.</li>
      <li>In a conflict, we will support and listen to those with least power. We believe survivors.</li>
    </ul>

    <p><b><i>Affinity groups:</i></b></p>
    <ul>
      <li>Affinity groups from a wide range of community backgrounds will be attending the Rally for Climate Justice. </li>
      <li>Affinity groups are autonomous and are encouraged to protest in the manner of their choosing. Tactics may range from quiet prayer to active blockading. </li>
      <li>Everyone has the right to protest in the manner of their choosing, and tone policing (critiquing or attempting to suppress someone’s emotions) will not be tolerated.</li>
      <li>Individuals or groups who attempt to control, or are negative towards, other people’s manner of protest may be asked to leave.</li>
      <li>Affinity groups are expected to affirm the rally principles: that we acknowledge mana whenua as rightful kaitiaki with ongoing tino rangatiratanga; that this action calls for climate justice - ending systems of oppression and prioritising solutions envisioned by communities most affected by climate change; and that a range of techniques are needed to create climate justice. </li>
    </ul>

    <p><b><i>Interactions with third parties and police:</i></b></p>
    <ul>
      <li>While the decision is ultimately up to individual activists, talking with security/police can put you and others at risk. Anything you say is likely to be used in intelligence profiles and can be used as evidence against you and others.</li>
      <li>Please be aware of this likelihood and refrain from identifying or giving information about other people (including perceived organisers) to the authorities without their explicit consent.</li>
    </ul>

    <p><b><i>Final notes:</i></b></p>
    <ul>
      <li>This policy has been accepted by consensus by the organising group for the Rally for Climate Justice in March 2018. It can be amended by the group at any time through a consensus process.</li>
      <li>While we cannot guarantee an environment free from all discrimination, the intention of our policy is to make people feel comfortable to approach us with any concerns.</li>
      <li>We hope this policy will help people understand their role in creating and maintaining a safer space.</li>
    </ul>

    <p><b><i>Acknowledgements:</i></b></p>
    <ul>
      <li>This safer spaces policy was informed by the respective policies of Auckland Peace Action and Peace Action Wellington. Thank you for your mahi.</li>
    </ul>
  </article>
</div>
