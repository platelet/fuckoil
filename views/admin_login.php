<!-- Main -->
<div id="main">
  <section>
    <h2>ADMIN LOGIN</h2>
    <form class="alt" method="post" action="/admin">
      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label for="password">Password</label>
          <input type="password" name="password" id="password" />
        </div>
      </div>
    </form>
  </section>
</div>
