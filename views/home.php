<!-- Main -->
<div id="main">

  <!-- Featured Post -->
    <article class="post featured">
      <header class="major">
        <h2>JOIN US FOR A WEEK OF CLIMATE ACTION</h2>
      </header>
      <p>Please join us for a Rally and blockade of the Oil Summit on 27th March at the TSB Arena! This will be one event in a week of climate action. From information days to NVDA trainings to a flower power rally, there are so many ways to get involved in the fight for climate justice!</p>

      <button onclick="window.location = '/events';"><a href="/events">Find out more about the week of action</a></button>
      <br>
      <br>

      <p>On 26-28 March 2018 the oil and gas industry is holding its annual business conference. Government ministers are lobbied, businesses network and oil and gas exploration permits are announced.</p>
      <p>Join the building movement for climate justice and a fair and sustainable future. Help us stop the Oil Summit in Wellington, 26th-28th March 2018. Now is the time for a just transition away from fossil fuels – not exploration for new oil and gas.</p>
      <p>We will be holding a rally on 27th March, and there will also be events on the 26th. Exact times and locations to follow.</p>

      <iframe width="720" height="480" src="https://www.youtube.com/embed/z1cSANwWzjY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

      <br>
      <br>

      <h2 class="align-left">What can you do?</h2>
      <p><strong>Make friends with us!</strong></p>
      <p>If you have a group who would be interested in hearing more about how to join the blockade or learn more about the oil industry, <a href="mailto:oilfreewellington@gmail.com">give us a shout!</a></p>
      <p><strong>Spread the word!</strong></p>
      <p>Get your friends together and come as a group, decide on an action you would like to take on the day, any messages you’d like to bring to the oil companies, any props, banners etc you would like to make, or any action training or planning you might want to do (we can help with this). You can check out this <a href="https://issuu.com/earthfirstjournal/docs/dam_3rd_edition" target="_blank">Earth First direct action training manual</a> for inspiration, or come to our Non-Violent Direct Action training (<a href="https://www.facebook.com/events/1954197434897139/">info on nvda</a>).</p>
      <p>You can also join the <a href="https://www.facebook.com/events/359022637895815/" target="_blank">Facebook event</a> and invite all your friends!</p>
      <p><strong>Book the day off! 27th March</strong></p>
      <p>Have you booked the day off work yet? Put it in your diary now to make sure you can be there on the day to stand up against the oil industry. You can also join the Facebook event for updates.</p>
      <p><strong>Book your travel now!</strong></p>
      <p>If you're planning to travel down by bus or plane, book your tickets now while they're relatively cheap, or keep an eye out for a good deal.</p>
      <p><strong>Can you chip in?</strong></p>
      <p>Let's make sure everyone can make it to the blockade. Please check out our <a href="https://givealittle.co.nz/cause/rally-for-climate-justice-blockade-the-oil-summit" target="_blank">Givealittle page</a> for a quick, secure way to donate.</p>

      <!-- <ul class="actions">
        <li><a href="#" class="button big">Join the group</a></li>
        <li><a href="#" class="button big">Pledge to Act</a></li>
        <li><a href="#" class="button big">Save the Date</a></li>
      </ul> -->
    </article>

</div>

<!-- Footer -->
<footer id="footer">
  <section style="text-align: center;">
    <h3>Groups commited to this kaupapa and action</h3>
    <div style="display: inline-block; margin: 5px;">
      <img src="images/ofw.png" alt="" width="100">
      <p>Oil Free Wellington</p>
    </div>
    <div id="copyright">
      <ul><li>anti-&copy;</li></ul>
    </div>
  </section>
</footer>
