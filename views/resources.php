<!-- Main -->
<div id="main">
  <section>
    <h2>Resources <small>- click to download</small></h2>
    <div class="box alt">
			<div class="row 50% uniform">
				<div class="4u">
          <a href="https://s3-ap-southeast-2.amazonaws.com/grec-froc-m5x2-5fgg67/assets/resources/poster-a3-min.pdf" target="_blank">
            <span class="image fit">
              <img src="https://s3-ap-southeast-2.amazonaws.com/grec-froc-m5x2-5fgg67/assets/resources/poster-a3-min.png" alt="Rally for climate justice poster">
            </span>
          </a>
        </div>
				<div class="4u">
          <a href="https://s3-ap-southeast-2.amazonaws.com/grec-froc-m5x2-5fgg67/assets/resources/flyer-a4-min.pdf" target="_blank">
            <span class="image fit">
              <img src="https://s3-ap-southeast-2.amazonaws.com/grec-froc-m5x2-5fgg67/assets/resources/flyer-a4-min.png" alt="Rally for climate justice flyer">
            </span>
          </a>
        </div>
        <div class="4u">
          <a href="https://s3-ap-southeast-2.amazonaws.com/grec-froc-m5x2-5fgg67/assets/resources/Safer+Spaces+Petroleum+Summit.pdf" target="_blank">
            <span class="image fit">
              <img src="https://s3-ap-southeast-2.amazonaws.com/grec-froc-m5x2-5fgg67/assets/resources/Safer+Spaces+Petroleum+Summit.png" alt="Safer Spaces Policy">
            </span>
          </a>
        </div>
			</div>
		</div>
  </section>
</div>
