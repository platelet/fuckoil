<!-- Intro -->
<?php if ($page === 'home') { ?>
<div id="intro">
  <h1>Rally for Climate Justice!</h1>
  <h2 style="font-size: 3.5em;">Blockade the Oil Summit!</h2>
  <h2>March 26th - 28th, Wellington, 2018</h2>
  <p>Stand up for climate justice and get in the way of oil business as usual.</p>
  <ul class="actions">
    <li><a href="#header" class="button icon solo fa-arrow-down scrolly">Continue</a></li>
  </ul>
</div>
<?php } ?>

<!-- Header -->
<header id="header">
  <?php if ($page === 'home') { ?>
    <a href="/" class="logo">Rally for Climate Justice</a>
  <?php } else { ?>
    <a href="/" class="logo">Rally for Climate Justice<br /><?php echo $page ?></a>
  <?php } ?>
</header>

<!-- Nav -->
<nav id="nav">
  <ul class="links">
    <li <?php if ($page === 'home') echo 'class="active"'; ?>><a href="/">Home</a></li>
    <li <?php if ($page === 'events') echo 'class="active"'; ?>><a href="/events">Events</a></li>
    <li <?php if ($page === 'about') echo 'class="active"'; ?>><a href="/about">About</a></li>
    <li <?php if ($page === 'principles') echo 'class="active"'; ?>><a href="/principles">Principles</a></li>
    <li <?php if ($page === 'safer') echo 'class="active"'; ?>><a href="/safer_spaces">Safer Spaces</a></li>
    <li <?php if ($page === 'register') echo 'class="active"'; ?>><a href="/register">Register</a></li>
    <li <?php if ($page === 'resources') echo 'class="active"'; ?>><a href="/resources">Resources</a></li>
  </ul>
  <ul class="icons">
    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
    <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
  </ul>
</nav>
