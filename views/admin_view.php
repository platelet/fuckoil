<!-- Main -->
<script>
  function download() {
    var csv = 'data:text/csv;charset=utf-8,<?php echo $csv ?>';
    var encodedUri = encodeURI(csv);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "report.csv");
    link.click();
  }
</script>
<div id="main">
  <section>
    <h2>ADMIN</h2>
    <button onclick="download()">Download Data as CSV</button>
    <br>
    <br>
    <table>
      <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Region</th>
        <th>Transport</th>
        <th>Accommodation</th>
        <th>Created At</th>
      </thead>
      <tbody>
        <?php foreach ($data as $row) { ?>
        <tr>
          <td><?php echo $row['name']; ?></td>
          <td><?php echo $row['email']; ?></td>
          <td><?php echo $row['phone']; ?></td>
          <td><?php echo $row['region']; ?></td>
          <td><?php echo $row['transport']; ?></td>
          <td><?php echo $row['accommodation']; ?></td>
          <td><?php echo $row['createdAt']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </section>
</div>
