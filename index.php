<?php

  require 'flight/Flight.php';
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  require 'PHPMailer/src/Exception.php';
  require 'PHPMailer/src/PHPMailer.php';
  require 'PHPMailer/src/SMTP.php';

  //restart any current browser sessions
	session_start();

	/* MYSQL DATABASE CONNECTION */
	//database creditails

	// define('DB_USER', 'root');
	// define('DB_PASS', '');
	// define('DB_NAME', 'fuckoil');
	// define('DB_HOST', 'localhost');
  // define('ADMIN_PASS', 'admin');
  define('DB_USER',getenv('DATABASE_USER'));
	define('DB_PASS',getenv('DATABASE_PASSWORD'));
	define('DB_NAME',getenv('DATABASE_NAME'));
	define('DB_HOST', 'mysql');
  define('ADMIN_PASS',getenv('ADMIN_PASSWORD'));

  define('EMAIL_PASS', getenv('EMAIL_PASS'));

	//connect to MySQL
	$r = mysql_connect(DB_HOST, DB_USER, DB_PASS);
	$r2 = mysql_select_db(DB_NAME);

  Flight::set('flight.views.path', 'views/');

  Flight::route('/.well-known/acme-challenge/@challengeHash', function($challengeHash) {
    echo $challengeHash.'.cy_rEkpqrMWh215qVnL7K9RLXbBpK5Kl7ubvuWvJWl4';
  });

  Flight::route('/', function(){
    Flight::render('header', array('page' => 'home'), 'header_content');
    Flight::render('home', array(), 'body_content');
    Flight::render('layout', array('title' => 'Stop the Oil Summit'));
  });

  Flight::route('/about', function(){
    Flight::render('header', array('page' => 'about'), 'header_content');
    Flight::render('about', array(), 'body_content');
    Flight::render('layout', array('title' => 'About'));
  });

  Flight::route('/principles', function () {
    Flight::render('header', array('page' => 'principles'), 'header_content');
    Flight::render('principles', array(), 'body_content');
    Flight::render('layout', array('title' => 'Principles'));
  });

  Flight::route('/safer_spaces', function () {
    Flight::render('header', array('page' => 'safer'), 'header_content');
    Flight::render('safer_spaces', array(), 'body_content');
    Flight::render('layout', array('title' => 'Safer Spaces Policy'));
  });

  Flight::route('/events', function () {
    Flight::render('header', array('page' => 'events'), 'header_content');
    Flight::render('events', array(), 'body_content');
    Flight::render('layout', array('title' => 'Week of Climate Justice'));
  });

  Flight::route('GET /register', function () {
    Flight::render('header', array('page' => 'register'), 'header_content');
    Flight::render('register', array(), 'body_content');
    Flight::render('layout', array('title' => 'Register'));
  });
  Flight::route('GET /registered', function () {
    Flight::render('header', array('page' => 'registered'), 'header_content');
    Flight::render('registered', array(), 'body_content');
    Flight::render('layout', array('title' => 'Registered'));
  });

  Flight::route('GET /admin', function () {
    if ($_SESSION['user_defined_admin_in'] == 'confirmed') {
      $query = "SELECT name, email, phone, region, transport, accommodation, createdAt FROM registers WHERE 1";
      $res = mysql_query($query);
      $data = [];
      $csv = 'name,email,phone,region,accommodation,transport,createdAt\n';
      while ($row = mysql_fetch_assoc($res)) {
        $csv .= $row['name'].','.$row['email'].','.$row['phone'].','.$row['region'].','.$row['accommodation'].','.$row['transport'].','.$row['createdAt'].'\n';
        array_push($data, $row);
      }
      Flight::render('header', array('page' => 'admin'), 'header_content');
      Flight::render('admin_view', array('data' => $data, 'csv' => $csv), 'body_content');
      Flight::render('layout', array('title' => 'Admin'));
    } else {
      Flight::render('header', array('page' => 'admin'), 'header_content');
      Flight::render('admin_login', array(), 'body_content');
      Flight::render('layout', array('title' => 'Admin Login'));
    }
  });
  Flight::route('POST /admin', function () {
    $request = Flight::request();
    $form_data = $request->data;
    $password = $form_data->password;
    if ($password == ADMIN_PASS) {
      $_SESSION['user_defined_admin_in'] = 'confirmed';
      Flight::redirect('/admin');
    } else {
      die('permission denied');
    }
  });

  function sendEmail($name, $email) {
    $mail = new PHPMailer(true);
    try {
      //Server settings
      $mail->SMTPDebug = 2;
      $mail->isSMTP();
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->Username = 'oilfreewellington@gmail.com';
      $mail->Password = EMAIL_PASS;
      $mail->SMTPSecure = 'tls';
      $mail->Port = 587;

      //Recipients
      $mail->setFrom('oilfreewellington@gmail.com', 'Oil Free Wellington');
      $mail->addAddress($email, $name);

      $mail->isHTML(true);
      $mail->Subject = 'Together we can shut down the Oil Summit - Important Information';
      // $body = 'Kia ora,<br/><br/>Awesome, you\'ve registered to be part of the Rally for Climate Justice! Data will be stored securely and deleted after the summit. Below you\'ll find some handy information.<br/><br/>';
      // $body .= '**PLEASE NOTE DATE CHANGE - THE RALLY FOR CLIMATE JUSTICE WILL NOW TAKE PLACE ON 27TH MARCH**</br></br>Please could you do one more thing and spread the word? You could share info about the Rally on <a href="http://www.twitter.com/home?status=It\'s never been more important to stand up to the oil and gas industries. Join the Rally for Climate Justice and blockade the oil summit on 26th March in Wellington! www.rallyforclimatejustice.nz #ClimateJustice">Twitter</a> or <a href="https://www.facebook.com/OilFreeWellington">Facebook</a>, find print-ready <a href="http://www.rallyforclimatejustice.nz/resources">posters and flyers</a> on our website, or just talk to friends, family or colleagues.<br/><br/>';
      // $body .= '<b>Blockade details</b><br/><br/>We have just uncovered information that the real business of the conference will begin on Tuesday, 27 March. Therefore, we have taken the decision to move the Rally for Climate Justice to that day.<br/><br/>We appreciate that some of you have already made plans to attend a Monday event, booking travel and time off work. We really appreciate the time you\'ve been willing to dedicate to standing up for climate justice, and would ask you to <b>please rearrange your plans if you can.</b> It\'s vital that we get the maximum number of people to disrupt the conference on Tuesday the 27th.<br/><br/>PEPANZ haven\'t publicly released the venue yet, as they are scared of protestors like us standing up to them. However, they haven\'t been able to keep it quite secret enough! We\'ll be publicising the venue location very soon, but we can tell you it\'s in central Wellington.</br></br><b>Things to bring</b><br/><br/>Remember, a good blockade is a prepared blockade! We aim to be there for the whole day so bring what you need to keep comfortable. We recommend bringing sunscreen, snacks, water, warm clothes, a cushion, more snacks, wet weather gear (unpredictable Wellington!) placards and banners, any medications you need, games and yet more snacks.<br/><br/>Remember to take breaks from the blockade when you need or switch roles with another person.<br/><br/><b>NVDA training</b><br/><br/>Wellington Non-Violent Direct Action training will take place on Sunday 18th March, 10am at Newtown Community Centre. There will also be training provided in the immediate run-up to the Rally/blockade. Training is also taking part in Christchurch and Auckland.</br></br>We\'ll share skills on how to be part of an action in a way that\'s effective, empowering, and as safe as possible.<br/><br/><b>More about us and our kaupapa</b><br/><br/>We\'re a new group made up of members of Oil Free Wellington plus other activists.<br/><br/>Our kaupapa is centred on climate justice, beginning from a recognition that those most affected by climate change are the least responsible for it. We acknowledge mana whenua as the rightful kaitiaki of Aotearoa, and recognise that tino rangatiratanga was never ceded. We recognise the urgency of acting on climate change and momentum built around this over the years. <a href="http://www.rallyforclimatejustice.nz/principles">Our full kaupapa can be found on our website</a>.<br/><br/><b>Tell us a bit more about you!</b><br/><br/>If you have specific skills you\'d like to offer us, please email us at <a href="mailto:oilfreewellington@gmail.com">oilfreewellington@gmail.com</a> and let us know!<br/><br/>';
      // $body .= 'We\'re looking for people with experience in making delicious food, first aid, legal expertise, music, video, and photography, or anything else that you\'d like to offer.<br/><br/>Please also let us know if you\'d like to coordinate a team of people on the day, or if you\'re part of a group that would like to be assigned to blockade an entrance to the venue.<br/><br/><b>What can I do right now?</b><br/><br/>The best thing you can do right now is share this event far and wide! The more people who come along, the more successful and fun the blockade will be. You could share info about the Rally on <a href="http://www.twitter.com/home?status=It\'s never been more important to stand up to the oil and gas industries. Join the Rally for Climate Justice and blockade the oil summit on 26th March in Wellington! www.rallyforclimatejustice.nz #ClimateJustice">Twitter</a> or <a href="https://www.facebook.com/OilFreeWellington">Facebook</a>, find print-ready <a href="http://www.rallyforclimatejustice.nz/resources">posters and flyers</a> on our website, or just talk to friends, family or colleagues and encourage them to come along with you.<br/><br/>You can stay updated with Oil Free Wellington on <a href="https://twitter.com/OilFreeWelly">Twitter</a> and <a href="https://www.facebook.com/OilFreeWellington">Facebook</a><br/><br/>If you have any questions, you can email us at <a href="mailto:oilfreewellington@gmail.com">oilfreewellington@gmail.com</a>.<br/><br/> It\'s never been more important to stand up to the oil and gas industries - so please spread the word far and wide!<br/><br/>The Rally for Climate Justice crew';

      $body = 'Kia ora,<br/><br/>Thanks heaps for registering for the Rally for Climate Justice. We are getting really excited! The Rally is only days away and we are really appreciative of all the support we are receiving from people like you.<br/><br/>Please note, data will be stored securely and deleted after the summit.<br/><br/><b>Have a look <a href="https://www.rallyforclimatejustice.nz/events">here</a> to see all the things will be happening!</b> There is a week of climate justice with <a href="https://www.facebook.com/events/278743309324616/">Moving on from Oil and Gas</a> on Sunday and the <a href="https://www.facebook.com/events/214427575775482/">Opening Ceremony for the Rally</a> on Monday - and some affinity groups who have activities you can join with too. Organised separately, there will also be lots of discussion happening at the <a href="https://www.facebook.com/events/778042099070395/">M&amacr;ori Leadership Climate Summit</a> on the 24th and 25th March.<br/><br/>This is a bit of a <b>long</b> email. We hope it gives you all the information you need. But please feel free to get in touch if you have any questions at all.<br/><br/><a>Please keep sharing and inviting folk you know:</b> the more people we have the more successful the rally and blockade will be. Make sure they <a href="https://www.rallyforclimatejustice.nz/register">register</a>.<br/><br/>And if you can help out to cover costs like accommodation and food - here is our <a href="https://givealittle.co.nz/cause/rally-for-climate-justice-blockade-the-oil-summit">Givealittle</a>.<br/><br/>';
      $body .= '<b>About the blockade:</b><br/><br/>On <b>Monday evening</b>, we will have a pot luck dinner at the <b>briefing</b> venue, the <a href="https://www.google.co.nz/maps/place/Wellington+Central+Baptist+Church/@-41.287572,174.774263,15z/data=!4m5!3m4!1s0x0:0x929e7f6d49ea0515!8m2!3d-41.287572!4d174.774263">Wellington Central Baptist Church, Boulcott Street</a>. The briefing begins at 6pm. Dinner will be provided after the briefing, followed by a short NVDA training for those who wish to stay for it. There will also be a chance to form affinity groups and choose ways participate at the Rally in a way that is comfortable for you. There will be a final briefing at 7am at <a href="https://goo.gl/maps/gBVKJMqPnfD2">Frank Kitts</a> Park on Tuesday.<br/><br/>It\'s really important that you read and understand our safer spaces policy, found <a href="https://www.rallyforclimatejustice.nz/safer_spaces">here</a><br/><br/>There will be food and hot drinks throughout the day.<br/><br/><b>Things to bring:</b><br/><br/><ul><li>Food and drink. We will have food being prepared at the blockade but having snacks never goes amiss. There is a water source close to the TSB Arena</li><li>Layers of clothes for the changing Wellington weather; a hat; sunscreen</li><li>A cushion, or something to sit on</li><li>Placards, signs, or banners that get your message across - why YOU think it\'s important to stand up for climate justice!</li><li>An idea of how you would like to participate in the rally/blockade (This will be more fully discussed at the Monday briefing)</li><li>A buddy. It\'s important to have support at protests, especially if you are planning on blockading. Stick together, keep checking in emotionally, and keep communicating throughout the day. There will be time to connect at the Monday briefing and early Tuesday morning.</li><li>And if you can, bring along things that could help at the blockade - like banners, rope etc. Get in touch if you have any questions.</li></ul><br/><br/><b>Tuesday evening debrief</b> begins at 5pm at the <a href="https://goo.gl/maps/ZZp7NK8MXFt">Sustainability Trust, 2 Forresters Lane</a>. There will be hot drinks and snacks, and we will have dinner either there or back at the accommodation.<br/><br/><b>Information for people coming from out of town:</b><br/><br/><b>Accommodation</b><br/><br/>';
      $body .= 'We have two options for accommodation offering marae-style sleeping, and they are available from Sunday evening to Wednesday morning:<br/><br/>1. <a href="https://www.google.co.nz/maps/place/Cook+Island+Hall/@-41.3129101,174.7746276,17z/data=!4m5!3m4!1s0x0:0x233515e4d209052a!8m2!3d-41.313583!4d174.7737908">The Cook Island Society Hall</a>, 220 Hanson Street Newtown. This venue is not accessible.Unless you have accessibility requirements, we recommend you stay here.<br/><br/>2. <a href="https://goo.gl/maps/TFjuzs2bUnS2">Berrigan House</a>,4 Kelburn Parade, Kelburn. This venue is accessible, and although up the hill, it is well connected with public transport. There is only space for 20 people at Berrigan House, so we will prioritise people with accessibility requirements. If there is a family with young children, Berrigan House have also offered a room (which is upstairs)<br/><br/>For both accommodation options, please bring: a pillow, pillow case, sheet, sleeping bag and towel.<br/><br/>Here is the information we need from you by 5pm Friday 23rd March - <a href="mailto:oilfreewellington@gmail.com">email oilfreewellington@gmail.com</a><br/><ul><li>If you haven\'t already, please let us know if you need accommodation and if you have dietary requirements.</li><li>Please let us know what day you are coming and which of our two accommodation options will suit you best.</li><li>We are doing our best to connect people with rides. Please get in touch if you have any questions about this too.</li></ul><br/><br/><b>Meals</b><br/><br/>Breakfast will be available each day at both accommodation options. On Sunday evening there will be a light dinner provided. We will eat a pot luck dinner (with a couple of big pots of soup provided) on Monday at the briefing, and on Tuesday, we’ll have dinner for out of towners at either the debrief, or back at the hall. Food not Bombs are kindly providing lunch for us all at the blockade.<br/><br/>Our plan is to provide food to meet all dietary requirements for out of towners - so please get in touch if you have anything we need to know.<br/><br/><b>Arriving/dropping gear off</b><br/><br/>We will be welcoming people at the Cook Island Society Hall from 5pm on Sunday 25th March.<br/><br/>If you are arriving on Monday 26th March, you can drop gear off from 10-11am or 2-3pm and after the briefing (7:30pm).<br/><br/>Berrigan House will be open on Sunday and Monday. It would be good to take your valuables with you.<br/><br/><b>Transport:</b><br/><br/>We ask that you arrange your own transport to and from the briefing. There will be some spaces in cars available, otherwise there are lots of buses from Newtown to town and back.<br/><br/><b>Tuesday morning:</b> Shuttles will leave the accommodation at around 6.30am. Please ensure you are up and ready with plenty of time.<br/>For public transport, the <a href="https://www.metlink.org.nz/timetables/bus/1">No. 1 Island Bay Bus</a> takes you to and from corner of Rintoul St and Stoke St, the closest stops to the Cook Island Society Hall. It is a 10-15 minute walk to the stop.<br/>You can catch this bus from Willis St - close to the Baptist Church (briefing), and Courtenay Place, close to the Sustainability Trust (debrief).<br/>There is a <a href="https://www.metlink.org.nz/timetables/bus/91">bus</a> from the airport to the centre of town.<br/>You can use this <a href="https://www.metlink.org.nz/#plan">journey planner</a> to plan your travels around the city.<br/><br/>';
      $body .= 'We\'re really looking forward to seeing you all in Wellington next week! Please let us know if you have any questions.<br/><br/>Ng&amacr; mihi nui<br/><br/>The organising crew<br/><br/>Rally for Climate Justice<br/><br/>';

      $mail->Body    = $body;

      $mail->send();
      return true;
    } catch (Exception $e) {
      return $mail->ErrorInfo;
    }
  }

  Flight::route('POST /register', function() {
    $request = Flight::request();
    $form_data = $request->data;

    $search = [';', '%', '*', "'", '"'];
    $replace = ['\;', '\%', '\*', "\'", '\"'];
    $name   = str_replace($search, $replace, $form_data->name);
    $email  = str_replace($search, $replace, $form_data->email);
    $phone  = str_replace($search, $replace, $form_data->phone);
    $region = str_replace($search, $replace, $form_data->region);
    $accom  = str_replace($search, $replace, $form_data->accom);
    $trans  = str_replace($search, $replace, $form_data->trans);

    $query = "INSERT INTO registers (name, email, phone, region, accommodation, transport) VALUES ('".$name."', '".$email."', '".$phone."', '".$region."', '".$accom."', '".$trans."');";

    $res = mysql_query($query);

    if ($res) {
      $e_res = sendEmail($name, $email);
      if ($e_res === true) {
        Flight::redirect('/registered', 402);
      } else {
        die($e_res);
        Flight::redirect('/error', 401);
      }
    } else {
      Flight::redirect('/error', 401);
    }
  });

  Flight::route('/resources', function () {
    Flight::render('header', array('page' => 'resources'), 'header_content');
    Flight::render('resources', array(), 'body_content');
    Flight::render('layout', array('title' => 'Resources'));
  });

  Flight::start();

?>
